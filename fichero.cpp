
/* FGETC PARA LEER UNO A UNO LOS CARACTERES */
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
	char aux;
FILE *f;

f = fopen("Texto.txt", "r"); // que fichero se abre y la forma de abrirlo 
	if (f == NULL){
		printf("No se ha podido leer el fuchero\n");
	}

	while(aux != EOF){  //Empieza a programar EOF (End of File)
			aux = fgetc(f); //Lee caracter a caracter
			printf("%c", aux);
	}

	printf("\n");

	fclose(f); // Siempre hay que ponerlo para limpiar.
	return 0;
}