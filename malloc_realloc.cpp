#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

/*
 * +-----+             +-------+
 * |     |             |       |
 * |  ---+------------>|   ----+---->Guille
 * |     |             |       |
 * +-----+             +-------+
 *                     |       |
 *                     |   ----+---->Mario
 *                     |       |
 *                     +-------+
 *                     |       |
 *                     |   ----+---->Bernabe
 *                     |       |
 *                     +-------+
 *                     |       |
 *                     |   ----+---->Ramon
 *                     |       |
 *                     +-------+
 * */
int
main (int argc, char *argv[])
{

    char **amigos = NULL; //puntero a un puntero de un caracter
    char **p; 
    int input = 0; //si es 0 no hemos leido nombre y si es 1 si
    int nnombres = 0;


    /* ENTRADA DE DATOS */
    do {
        system ("clear");
        amigos = (char **) realloc (amigos, (nnombres + 1) * sizeof (char *)); //realloc funciona como malloc al tener en amigos null. En la primera vuelta amigos es NULL. // la variable amigos dice que en esa variable tiene que entrar lo siguiente. 
        printf ("Amigo: ");
        input = scanf ("%m[a-z A-Z]", amigos + nnombres++ ); // leeme cualquier conjunto de caracteres de la a-z  sin espacios y guarda en amigos 
        __fpurge (stdin);       // sacar salto de linea.     // Al leer nombre vuelve. CON EL AMIGOS++ el realloc amplia al siguiente.
    } while (input);                                         // Se realiza el malloc para reservar espacio para los caracteres y realloc para el espacio en la memoria.

    /* SALIDA DE DATOS */
    system ("clear");
    p = amigos; 
    while (*p != NULL){             //Si lo que apunta p es diferente de null escribimos los string del puntero de p y hacemos 
        printf ("%s\n", *p);
        p++;
    }

    /* LIMPIEZA */
    for (char **p=amigos; *p!=NULL; p++)
        free (*p); //liberar memoria
    free (amigos); //liberar memoria

    return EXIT_SUCCESS;
}
//nnombre ++ primero utiliza el nnombres y luego lo incrementa.
