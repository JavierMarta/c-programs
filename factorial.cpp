/*   Factorial  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long  factorial(long numero){
if (numero<=1){
	return 1;
}else{
	return (numero * factorial (numero-1)); //Si no es menor o igual que 1 se realiza el numero por el factorial -1 del mismo numero
}
}

int main(int argc, char const *argv[])
{
	int numero;
	printf("Escribe un numero\n");
	scanf("%i",&numero);
	for (int i=0;i<=numero;i++){

printf("El numero es %ld \n", factorial(i)); //se muestra i para que vaya cambiando cada numero
}

	return EXIT_SUCCESS;
}