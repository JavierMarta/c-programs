
/*   Factorial dividiendo entre 1  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

float  factorial(float numero){ // float para los decimales 
if (numero<=0)
	return 1;

	return (1/ numero + factorial (numero-1)); // se divide 1 entre el numero y se le suma su factorial.
}

int main(int argc, char const *argv[])
{
	int numero;
	printf("Escribe un numero\n");
	scanf("%i",&numero);
	for (int i=numero;i>=0;i--){ // lo hago descendente para ver mejor el proceso. 

printf("El numero es (%i)  %.2f \n",i, factorial(i)); //se muestra i para que vaya cambiando cada numero
}

	return EXIT_SUCCESS;
}
+