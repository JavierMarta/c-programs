#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


char
lo_que_quiera_el_usuario (char  s, char *room)
{

    return room[s -1] < 0 ? 0 : 1; /* si  s-1 si es menor que 0 entonces(?)
                                      devuelve un 0 sino(:) un 1 */
}

int
main (int argc, char *argv[])
{
  /* Declaracion de variables */

    char *room; /*La direccion de un entero en ROOM*/
    char summit = 0; /* Cima */

    room = (char * ) malloc (sizeof (char)); /* La direccion de un room
                                            entra en un entero (4 bits)  */

  /*  Entrada de datos  */

    do{
        printf ("Nombre de mejor amigo:\n ");
        scanf ("%c", &room[summit++]);
        room = (char *) realloc ( room,(summit +1) * sizeof (char) ); /* Amplia un entero
                                                                     cada vez que añades otro  */
    }

    while (lo_que_quiera_el_usuario(summit, room));
    summit--; /* si el usuario pone un numero menor que 0 se baja la cima y no aparece el
                 ultimo digito negativo en la siguiente lista.   */

    /* Salida de datos  */

    printf ("\n");
    for (unsigned i=0; i<summit; i++){
        printf("%s, %s\n", i+1, room[i]);
    printf ("\n");
    }

    free (room);

    return EXIT_SUCCESS;
}

