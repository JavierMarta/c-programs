
/*CÁRACTER RECURSIVO*/

  

 #include <stdio.h>
 #include <stdlib.h>

 const char *palabras = "Hola";

 void imprime(const char *n ){
 	if(*n == '\0')
     	return;
 	imprime (n+1);
 	printf ("%c", *n);
 }

 int main() {
     	imprime (palabras);
     	printf("\n");
 
      	return EXIT_SUCCESS;
 
  }

